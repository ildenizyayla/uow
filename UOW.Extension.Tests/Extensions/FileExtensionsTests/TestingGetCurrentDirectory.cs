﻿namespace UOW.Extension.Tests.Extensions.FileExtensionsTests
{
    using System;
    using System.IO;
    using UOW.Extension.Extensions;
    using Xunit;

    public class TestingGetCurrentDirectory : TestBase
    {
        private const string _ExpectedContent = "Lorem Ipsum";
        private string _ReadContent;
        private Exception _ThrownException;

        public override void Given(){}

        public override void When()
        {
            try
            {
                _ReadContent = 
                    File.ReadAllText("TestFile".GetCurrentDirectory("txt", "Helper"));
            }
            catch (Exception ex)
            {
                _ThrownException = ex;
            }
            
        }

        [Fact]
        public void FileWasReadWithNoException()
        {
            Assert.Equal(_ExpectedContent, _ReadContent);
            Assert.Null(_ThrownException);
        }
    }
}
