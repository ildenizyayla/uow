namespace UOW.Extension.Tests
{
    public abstract class TestBase
    {
        public abstract void When();
        public abstract void Given();

        public TestBase()
        {
            Given();
            When();
        }
    }
}