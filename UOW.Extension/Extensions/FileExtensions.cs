﻿namespace UOW.Extension.Extensions
{
    using System.IO;

    public static class FileExtensions
    {
        public static string GetCurrentDirectory(this string fileName, string fileExtension, string subPath = null) =>
            Path.Combine(GetBasePath(), subPath, $"{fileName}.{fileExtension}");

        public static string GetBasePath()
        {
            var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            return path.Substring(6, path.Length - 6);
        }
    }
}
