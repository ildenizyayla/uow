# Unit of Work
A Unit of Work implementation using Dapper with a custom migration builder.

1. [Dapper](https://github.com/StackExchange/Dapper/ "Go") as micro ORM.
2. [SimpleCRUD](https://github.com/ericdc1/Dapper.SimpleCRUD, "Go") for extending Dapper functionalities. 
3. [xUnit](https://xunit.github.io/ "Go") and [Moq](https://github.com/moq/moq4 "Go") for unit testing.

## Getting Started

### Prerequisites
.Net Core runtime and a SQL server instance with target database is needed.

### Installation
The application does not require any pre-installation, other than a internet connection to restore nuget packages.

## Project Structure
* **Web** project sets up the instance and contains business logic to serve to the client.
* **Data** project holds necessary entity meta as well as repositories for data storage.
* **.{ProjectName}.Tests** projects contains tests related to {ProjectName} project.

## Tests
Few tests were added.

## Deployment
No deployment implementation was provided.

## Entity Migration
Migration project can be enhanced. However, a simple implementation using JSON files was added.

## License
This project is not licensed.