﻿namespace UOW.Migration
{
    using Dapper;
    using Entities;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using UOW.Extension.Extensions;
    using System.IO;

    public static class MigrationBuilder
    {
        private static SqlConnection _Connection;
        private static List<MigrationObject> _MigrationObj;

        public static async Task<bool> RunMigration(string connectionString, string version)
        {
            _Connection = new SqlConnection(@connectionString);
            await ParseMigrationScript(version);
            await RunTransaction();

            return true;
        }

        private static async Task ParseMigrationScript(string fileName) =>
            _MigrationObj = JsonConvert.DeserializeObject<List<MigrationObject>>(await ReadMigrationFile(fileName));

        private static async Task<string> ReadMigrationFile(string fileName) =>
            await File.ReadAllTextAsync(fileName.GetCurrentDirectory("json", "Migrations"));

        public static async Task RunTransaction()
        {
            if(_Connection.State != System.Data.ConnectionState.Open)
                await _Connection.OpenAsync();

            using (var trans = _Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                try
                {
                    foreach(var item in _MigrationObj)
                    {
                        Console.WriteLine($"----- Running {item.ScriptType} -----");
                        foreach(var script in item.Scripts)
                        {
                            Console.WriteLine($"Script => {script.ObjectName}");
                            await _Connection.ExecuteAsync(script.Script, transaction: trans);
                        }
                    }
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
                finally
                {
                    if(_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
            }
        }
    }
}
