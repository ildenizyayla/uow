﻿namespace UOW.Migration.Entities
{
    internal class ScriptObject
    {
        public string ObjectName { get; set; }
        public string[] DbScript { get; set; }
        public string Script => string.Join("\n", DbScript);
    }
}
