﻿namespace UOW.Migration.Entities
{
    using Enums;
    using System.Collections.Generic;

    internal class MigrationObject
    {
        public DbScriptType ScriptType { get; set; }
        public List<ScriptObject> Scripts { get; set; }
    }
}
