﻿namespace UOW.Data.Repositories
{
    using System.Data;
    using UOW.Data.Entities;
    using UOW.Data.Interfaces;

    internal class PostRepository: Repository<UserPost>, IPostRepository
    {
        public PostRepository(IDbTransaction transaction) : base(transaction) {}
    }
}
