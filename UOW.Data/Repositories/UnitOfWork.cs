﻿namespace UOW.Data.Repositories
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using UOW.Data.Interfaces;

    public class UnitOfWork: IUnitOfWork
    {
        #region Fields
        private IDbConnection _Connection;
        private IDbTransaction _Transaction;
        private IPostRepository _PostRepository;

        private bool _Disposed;
        #endregion

        public UnitOfWork(string connectionString)
        {
            _Connection = new SqlConnection(connectionString);
            _Connection.Open();
            _Transaction = _Connection.BeginTransaction();
        }

        #region IUnitOfWork Members

        public IPostRepository PostRepository {
            get { return _PostRepository ?? (_PostRepository = new PostRepository(_Transaction)); }
        }

        public void Commit()
        {
            try
            {
                _Transaction.Commit();
            }
            catch(Exception ex)
            {
                _Transaction.Rollback();
                throw ex;
            }
            finally
            {
                _Transaction.Dispose();
                resetRepositories();
                _Transaction = _Connection.BeginTransaction();
            }
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Private Methods
        private void resetRepositories()
        {
            _PostRepository = null;
        }

        private void dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (disposing)
                {
                    if (_Transaction != null)
                    {
                        _Transaction.Dispose();
                        _Transaction = null;
                    }
                    if (_Connection != null)
                    {
                        _Connection.Dispose();
                        _Connection = null;
                    }
                }
                _Disposed = true;
            }
        }

        ~UnitOfWork()
        {
            dispose(false);
        }
        #endregion
    }
}
