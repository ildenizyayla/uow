﻿namespace UOW.Data.Repositories
{
    using Dapper;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using UOW.Data.Entities;
    using UOW.Data.Interfaces;

    internal abstract class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private IDbTransaction _transaction;
        private IDbConnection _Connection => _transaction.Connection; //{ get { return _transaction.Connection; } }

        public Repository(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public async Task<T> ExecuteScalar(string sql, object param) =>
            await _Connection.ExecuteScalarAsync<T>(sql, param, _transaction);

        public async Task<T> QuerySingleOrDefault(string sql, object param) =>
            await _Connection.QuerySingleOrDefaultAsync<T>(sql, param, _transaction);

        public async Task<IEnumerable<T>> Query(string sql, object param = null) =>
            await _Connection.QueryAsync<T>(sql, param, _transaction);

        public async Task<int> Execute(string sql, object param) =>
            await _Connection.ExecuteAsync(sql, param, _transaction);

        public async Task<IEnumerable<T>> GetAll() =>
            await _Connection.GetListAsync<T>(_transaction);

        public async Task<IEnumerable<T>> GetPaged(
            int pageNumber,
            int rowsPerPage,
            string conditions,
            string orderBy,
            object parameters) =>
            await _Connection.GetListPagedAsync<T>(
                pageNumber,
                rowsPerPage,
                conditions,
                orderBy,
                parameters,
                _transaction);
        
        public async Task<T> Find(Guid Id) =>
            await _Connection.GetAsync<T>(Id, _transaction);

        public async Task<Guid> Add(T entity) =>
            await _Connection.InsertAsync<Guid, T>(entity, _transaction);

        public async Task AddAll(List<T> entities)
        {
            foreach (var entity in entities)
            {
                await _Connection.InsertAsync(entity, _transaction);
            }
        }

        public async Task<bool> Update(T entity) =>
            (await _Connection.UpdateAsync(entity, _transaction)) > 0;
        
        public async Task<bool> HardDelete(T entity) =>
            (await _Connection.DeleteAsync<T>(entity.Id, _transaction)) > 0;

        public async Task<bool> SoftDelete(T entity)
        {
            entity.Deleted = true;
            return await Update(entity);
        }

        public async Task<bool> HardDeleteAll(List<T> entities) => 
            (await _Connection.DeleteListAsync<T>(entities)) == entities.Count;

        public async Task<int> GetCount(string filter = null, object parameters = null) =>
            await _Connection.RecordCountAsync<T>($"{filter}", null, _transaction);
    }
}
