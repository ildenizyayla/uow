﻿namespace UOW.Data.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IRepository<T> where T : class
    {
        Task<T> ExecuteScalar(string sql, object param);

        Task<T> QuerySingleOrDefault(string sql, object param);

        Task<IEnumerable<T>> Query(string sql, object param = null);

        Task<int> Execute(string sql, object param);

        Task<IEnumerable<T>> GetAll();

        Task<IEnumerable<T>> GetPaged(int pageNumber, int rowsPerPage, string conditions, string orderBy, object parameters);

        Task<T> Find(Guid Id);

        Task<Guid> Add(T entity);

        Task AddAll(List<T> entities);

        Task<bool> Update(T entity);

        Task<bool> HardDelete(T entity);

        Task<bool> SoftDelete(T entity);

        Task<bool> HardDeleteAll(List<T> entities);

        Task<int> GetCount(string filter = null, object parameters = null);
    }
}
