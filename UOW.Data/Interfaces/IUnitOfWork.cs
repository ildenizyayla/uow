﻿namespace UOW.Data.Interfaces
{
    using System;

    public interface IUnitOfWork: IDisposable
    {
        IPostRepository PostRepository { get; }

        void Commit();
    }
}
