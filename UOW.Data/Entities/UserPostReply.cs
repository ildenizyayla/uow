﻿namespace UOW.Data.Entities
{
    using Dapper;
    using System;

    [Table("PostReply")]
    public class UserPostReply: BaseEntity
    {
        public Guid PostId { get; set; }

        public Guid? ParentId { get; set; }
        public string Message { get; set; }
    }
}
