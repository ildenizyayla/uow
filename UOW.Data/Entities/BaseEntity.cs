﻿namespace UOW.Data.Entities
{
    using Dapper;
    using System;

    public abstract class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        public string CreatedBy { get; set; }
        public bool Deleted { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
