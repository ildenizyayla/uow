﻿namespace UOW.Data.Entities
{
    using Dapper;

    [Table("Post")]
    public class UserPost: BaseEntity
    {
        public string PostSubject { get; set; }
        public string PostMessage { get; set; }

        [Editable(false)] 
        public string Replies { get; set; }
    }
}
